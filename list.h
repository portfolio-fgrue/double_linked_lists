#ifndef _LIST_H
#define _LIST_H

class Node:public List
{
private:
    Node *previous;
    int value;
    Node *next;
};

class List
{
private:
int count = 0;
Node* starter = {nullptr};

public:
    List(int value);
    List *append(int value);
    int get(int index);
    int size();
    bool remove(int index);
    void clearList();
    ~List();

};

#endif