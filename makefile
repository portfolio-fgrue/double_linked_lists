all: double_linked_lists.exe

double_linked_lists.exe: double_linked_lists.o list.o
	g++ -o double_linked_lists.exe double_linked_lists.o list.o

double_linked_lists.o: double_linked_lists.cpp
	g++ -c double_linked_lists.cpp

list.o: list.cpp
	g++ -c list.cpp

clean:
	rm double_linked_lists.o list.o double_linked_lists.exe